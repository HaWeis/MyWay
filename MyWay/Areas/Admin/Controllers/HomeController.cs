﻿using BLL;
using Wei_DB;
using Wei_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using MyWay.App_Start;

namespace MyWay.Areas.Admin.Controllers
{
    [LoginVerifyFilter(false)]
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }

        [LoginVerifyFilter(true)]
        public ActionResult Login()
        {
      
            return View();
        }
        [LoginVerifyFilter(true)]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UsersBLL bll = new UsersBLL();
                    var user = bll.Login(model.Email, model.Password);
                    if (user != null)
                    {
                        Session["User"] = user;
                        //return Content("<script>alert('邮箱或者密码不能为空');location.href='/Admin/Home/Login';</script>");          
                        return RedirectToAction("Index", "Home", new { area = "Admin" });
                    }
                    else
                    {
                        ModelState.AddModelError("", "用户名或密码错误！");
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                //Response.Redirect("/");
                Response.Write(ex);
                return new EmptyResult();
            }         
        }


        /// <summary>
        /// 注册页面
        /// </summary>
        /// <returns></returns>
        [LoginVerifyFilter(true)]
        public ActionResult Register()
        {
            return View();
        }
        [LoginVerifyFilter(true)]
        [HttpPost]
        public ActionResult Register(Wei_UsersModel user)
        {
            try
            {


                if (ModelState.IsValid)
                {
                    UsersBLL bll = new UsersBLL();
                    string res = bll.Register(user);
                    if (res == "成功")
                    {
                        return Content(Script("注册成功", "/Admin/Home/Login"));
                        //return RedirectToAction("Login", "Home", new { area = "Admin" });
                    }
                    else
                    {
                        return Content(Script(res, "/Admin/Home/Register"));
                    }
                }
                return View(user);
            }
            catch (Exception)
            {
                Response.Redirect("/");
                return new EmptyResult();
            }
        }

        public string Script(string msg,string url)
        {
            return "<script>alert('" + msg + "');window.location.href='" + url + "';</script>";
        }


        /// <summary>
        /// 忘记密码页面
        /// </summary>
        /// <returns></returns>
        [LoginVerifyFilter(true)]
        public ActionResult ForgotPassword()
        {
            return View();
        }



        public string LoginOut()
        {
            Session["User"] = null;
            return "退出成功";
        }


    }
}