﻿using System.Web.Optimization;

namespace MyWay.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/helsinki-blue/vendor/jquery/jquery-1.12.3.min.js",
                        "~/Scripts/layer/layer.js"));
            //基本的JS
            bundles.Add(new ScriptBundle("~/bundles/helsinki-blue/script").Include(
                        "~/Scripts/helsinki-blue/vendor/bootstrap/js/bootstrap.min.js",
                        "~/Scripts/helsinki-blue/vendor/nano-scroller/nano-scroller.js",
                        "~/Scripts/helsinki-blue/javascripts/template-script.min.js",
                        "~/Scripts/helsinki-blue/javascripts/template-init.min.js"));
            //统计图相关的js
             bundles.Add(new ScriptBundle("~/bundles/helsinki-blue/script-chart").Include(
                        "~/Scripts/helsinki-blue/vendor/toastr/toastr.min.js",
                        "~/Scripts/helsinki-blue/vendor/chart-js/chart.min.js",
                        "~/Scripts/helsinki-blue/vendor/magnific-popup/jquery.magnific-popup.min.js",
                        "~/Scripts/helsinki-blue/javascripts/examples/dashboard.js"));
            //基本的css
            bundles.Add(new StyleBundle("~/Scripts/helsinki-blue/css").Include(
                "~/Scripts/helsinki-blue/vendor/bootstrap/css/bootstrap.css",
                "~/Scripts/helsinki-blue/vendor/animate.css/animate.css",
                "~/Scripts/helsinki-blue/stylesheets/css/style.css"));

            //统计图的css
            bundles.Add(new StyleBundle("~/Scripts/helsinki-blue/chart-css").Include(
                "~/Scripts/helsinki-blue/vendor/toastr/toastr.min.css",
                "~/Scripts/helsinki-blue/vendor/magnific-popup/magnific-popup.css"));

        }
    }
}