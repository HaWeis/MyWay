﻿using MyWay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyWay.App_Start
{
    public class LoginVerifyFilter: ActionFilterAttribute
    {
        public bool Access { get; set; }
        private bool white = false;
        /// <param name="white">是否白名单</param>
        public LoginVerifyFilter(bool white = false)
        {
            this.white = white;
        }

    

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            FlashLogger.Debug("开始写了当前时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            if (white) return;
            HttpContextBase context = filterContext.HttpContext;
            HttpResponseBase Response = context.Response;
            HttpRequestBase Request = context.Request;
            HttpSessionStateBase Session = context.Session;
            if (Session == null || Session["User"] == null)
            {

                string sheader = Request.Headers["X-Requested-With"];
                if (sheader != null && sheader == "XMLHttpRequest")
                {
                    Response.Write("code:101");
                    filterContext.Result = new EmptyResult();
                    return;
                }
                else
                {
                    var url = context.Request.Url.ToString();
                    if (url.IndexOf("/ss/ImgProcess/ProcessRequest") >= 0)
                    {
                        Response.Write("code:101");
                        filterContext.Result = new EmptyResult();
                        return;
                    }
                    else
                    {
                        Response.Redirect("/");
                        filterContext.Result = new EmptyResult();
                        return;
                    }
                }
            }
        }

    }


    public class CustomerFilterAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            FlashLogger.Debug("异常时间："+ filterContext.Exception.Message.ToString()  + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            // 只操作未处理的异常
            // 程序中 try 里的异常 就不记录了
            if (!filterContext.ExceptionHandled)
            {
                // 不跳转直接返回404页面
                filterContext.HttpContext.Server.TransferRequest("~/error-404.html");
            }
        }
    }

}