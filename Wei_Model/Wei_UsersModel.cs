﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wei_Model
{
    public class Wei_UsersModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "昵称不能为空")]
        public string User_Name { get; set; }

        [Required(ErrorMessage = "Email不能为空")]
        [EmailAddress(ErrorMessage = "邮件格式错误")]
        public string Email { get; set; }

        [Compare("Password", ErrorMessage = "两次密码不一致")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "密码不能为空")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "最少6位,最大20位")]
        public string Password { get; set; }
        public int Role { get; set; }
        public System.DateTime? Create_Time { get; set; }
        public int Active { get; set; }
        public System.DateTime? Last_Login_Time { get; set; }
        public DateTime? Last_Update_Password_Time { get; set; }
        public int Is_OnLine { get; set; }

    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Email不能为空")]
        [EmailAddress(ErrorMessage = "邮件格式错误")]
        public string Email { get; set; }
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }

    }



}
